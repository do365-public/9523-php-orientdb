#!/usr/bin/env bash

export CMD_PATH=$(cd `dirname $0`; pwd)
export PROJECT_NAME="${CMD_PATH##*/}"

echo $PROJECT_NAME
cd $CMD_PATH

export LC_ALL=zh_CN.UTF-8
export PATH=/www/server/php/80/bin:$PATH
export COMPOSER_MEMORY_LIMIT=-1
composer install
composer update
